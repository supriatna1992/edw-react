
// export default App;
import React, {Component} from 'react';
import Content from './componets/layout/Content';
import Footer from './componets/layout/Footer';
import Header from './componets/layout/Header';
import Menu from './componets/layout/Menu';

class App extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div>
        <Header />
        <Menu />
        <Content />
        <Footer />
      </div> 
    );
  }
}

export default App;
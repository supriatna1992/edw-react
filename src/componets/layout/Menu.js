import React, { Component } from "react";

class Menu extends Component{
    render(){
        return(
            <aside className="main-sidebar sidebar-dark-primary elevation-4" style={{ panelAutoHeight:true }}>
                {/* Brand Logo */}
                <a href="index3.html" className="brand-link">
                    <img src={process.env.PUBLIC_URL + 'dist/img/AdminLTELogo.png'} alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{opacity: '.8'}} />
                    <span className="brand-text font-weight-light">e-Komunitas</span>
                </a>
                {/* Sidebar */}
                <div className="sidebar" style={{ height:80+'rem'}}>
                    {/* Sidebar user panel (optional) */}
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div className="image">
                        <img src={process.env.PUBLIC_URL + 'dist/img/user2-160x160.jpg'} className="img-circle elevation-2" alt="User Image" />
                    </div>
                    <div className="info">
                        <a href="#" className="d-block">Yusup Supriatna</a>
                    </div>
                    </div>
                    {/* Sidebar Menu */}
                    <nav className="mt-2">
                    <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        {/* Add icons to the links using the .nav-icon class
                                with font-awesome or any other icon font library */}
                        <li className="nav-item has-treeview menu-open">
                        <a href="#" className="nav-link active">
                            <i className="nav-icon fas fa-tachometer-alt" />
                            <p>
                            Dashboard
                            <i className="right fas fa-angle-left" />
                            </p>
                        </a>
                        <ul className="nav nav-treeview">
                            <li className="nav-item">
                            <a href="./index3.html" className="nav-link ">
                                <i className="far fa-circle nav-icon" />
                                <p>Pengunjung</p>
                            </a>
                            </li>
                            <li className="nav-item">
                            <a href="./index3.html" className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Pengguna</p>
                            </a>
                            </li>
                            <li className="nav-item">
                            <a href="./index3.html" className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Komunitas</p>
                            </a>
                            </li>
                        </ul>
                        </li>
                        <li className="nav-item">
                        <a href="pages/widgets.html" className="nav-link">
                            <i className="nav-icon fas fa-list" />
                            <p>
                            Komunitas Terdaftar
                            <span className="right badge badge-danger"></span>
                            </p>
                        </a>
                        </li>
                        <li className="nav-item has-treeview">
                            <a href="#" className="nav-link">
                                <i className="nav-icon fas fa-users" />
                                <p>
                                Data Warga
                                <i className="fas fa-angle-left right" />
                                <span className="badge badge-info right"></span>
                                </p>
                            </a>
                            <ul className="nav nav-treeview">
                                <li className="nav-item">
                                <a href="pages/layout/top-nav.html" className="nav-link">
                                    <i className="far fa-circle nav-icon" />
                                    <p>Warga Sekitar</p>
                                </a>
                                </li>
                                <li className="nav-item">
                                <a href="pages/layout/top-nav.html" className="nav-link">
                                    <i className="far fa-circle nav-icon" />
                                    <p>Keluarga Sekitar</p>
                                </a>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item has-treeview">
                            <a href="#" className="nav-link">
                                <i className="nav-icon fas fa-file" />
                                <p>
                                Laporan
                                <i className="fas fa-angle-left right" />
                                <span className="badge badge-info right"></span>
                                </p>
                            </a>
                            <ul className="nav nav-treeview">
                                <li className="nav-item">
                                <a href="pages/layout/top-nav.html" className="nav-link">
                                    <i className="far fa-circle nav-icon" />
                                    <p>Data Domisili</p>
                                </a>
                                </li>
                                <li className="nav-item">
                                <a href="pages/layout/top-nav.html" className="nav-link">
                                    <i className="far fa-circle nav-icon" />
                                    <p>Data Warga</p>
                                </a>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item has-treeview">
                            <a href="#" className="nav-link">
                                <i className="nav-icon fa fa-wrench" />
                                <p>
                                Setting
                                <i className="fas fa-angle-left right" />
                                <span className="badge badge-info right"></span>
                                </p>
                            </a>
                            <ul className="nav nav-treeview">
                                <li className="nav-item">
                                <a href="pages/layout/top-nav.html" className="nav-link">
                                    <i className="far fa-circle nav-icon" />
                                    <p>Role Akses</p>
                                </a>
                                </li>
                                <li className="nav-item">
                                <a href="pages/layout/top-nav.html" className="nav-link">
                                    <i className="far fa-circle nav-icon" />
                                    <p>Rukun Warga</p>
                                </a>
                                </li>
                                <li className="nav-item">
                                <a href="pages/layout/top-nav.html" className="nav-link">
                                    <i className="far fa-circle nav-icon" />
                                    <p>Rukun Tetangga</p>
                                </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    </nav>
                    {/* /.sidebar-menu */}
                </div>
                {/* /.sidebar */}
                </aside>

        );
    }
}
export default Menu;
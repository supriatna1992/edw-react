import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Layout from './Layout';
import Pengunjung from './componets/module/dashboard/Pengunjung';
// import Footer from './componets/layout/Footer';
// import Header from './componets/layout/Header';
// import Menu from './componets/layout/Menu';


// const routerList = [
//   {
//     'icon':'tachometer-alt',
//     'path':'/pengunjung',
//     // 'component':'DashboardPengunjung',
//     'name':'Dashboard',
//     'layout':'Layout',
//     'subs':[]
//   },
//   {
//     'icon':'list',
//     'path':'/komunitas',
//     'component':'Komunitas',
//     'name':'Komunitas Terdaftar',
//     'layout':'Layout',
//     'subs':[]
//   },
//   {
//     'icon':'users',
//     'path':'/datawarga',
//     'component':'DataWarga',
//     'name':'Data Warga',
//     'layout':'Layout',
//     'subs':[
//       {
//         'icon':'circle',
//         'path':'/warga',
//         'component':'Warga',
//         'name':'Warga Sektitar',
//         'layout':'Layout'
//       },
//       {
//         'icon':'circle',
//         'path':'/keluarga',
//         'component':'DataKeluarga',
//         'name':'Keluarga Sektitar',
//         'layout':'Layout'
//       }
//     ]
//   },

// ];


const AppRoute = ({component:Component, layout:Layout, ...rest})=>
(
    <Route {...rest} render={props=>(
        <Layout>
            <Component {...props}></Component>
        </Layout>
    )}>
        
    </Route>
)

class App extends Component{
  constructor(props){
    super(props);
  }
  
  render(){
    return(
      <div>
        <Router>
            <Switch>
                <AppRoute path='/' layout={Layout} component={Pengunjung} />
                <AppRoute path='/dashboard/pengunjung' layout={Layout} component={Pengunjung} />
            </Switch>
        </Router>
      </div> 
    );
  }
}

export default App;